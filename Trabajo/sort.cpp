#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <cctype>
using namespace std;
#include "sort.h"

Sorting::Sorting(){}

void Sorting::printlist(int tam, int *list) {
  for(int x = 0; x < tam; x++){
    cout << "Lista[" <<  x << "]: " << list[x] << " - ";
    if (x%3 == 0 && x != 0) {
      cout << endl;
    }
  }
  cout << endl;
}

/* Acá estará la seccion de ordenar por selección, en donde se ordenará
 * los numeros conseguidos, pero este orden no será tan rapido, pero el codigo
 * ocupa una cantidad menor de lineas y es bastante trivial
 */
void Sorting::Selection(int tam, int *list, string show) {
  int menor;
  int k;
  int i;
  int j;
  int list2[tam];
  cout << "Iniciando ..." << endl;
  /* se pasa el listado a otra lista, así se puede modificar y el originial se
     mantiene intacto, por si se quiere probar el otro tipo de ordenar */
  for(int x = 0; x < tam; x++){
    list2[x] = list[x];
  }
  cout << "Ordenando ..." << endl;
  /* Tiempo inicial de ordenar, de acá inicia el ordenar los numeros */
  chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();
  /* Este metodo de buscar consiste en buscar el numero menos y colocarlo al
     inicio de la lista que se tiene, por medio de ir comparando todo los
     numeros, y así se podrá conseguir ordenarlos de mayor a menor, pero toma
     un tiempo moderado */
  for (i = 0; i <= tam-2; i++) {
    menor = list2[i];
    k = i;

    for (j = i+1; j <= tam-1; j++) {
      if (list2[j] < menor) {
        menor = list2[j];
        k = j;
      }
    }
    list2[k] = list2[i];
    list2[i] = menor;
  }
  /* fin de ordenar, se toma el tiempo de nuevo */
  chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
  /* Se resta el tiempo final con el inicial, obteniendo el tiempo que se demoró */
  chrono::duration<double> diff = end-start;
  /* en el caso que el usuario le guste ver la lista ordenada, se le da la
     opción en el inicio, si el usuario dice Y o y, se mostrará toda la lista,
     pero mostrar la lista no afectará el tiempo en que se demoró en ordnelarla */
  if (show == "Y" || show == "y") {
    cout << "Ordenado por Selection: " << endl;
    printlist(tam, list2);
  }
  cout << "Tiempo demorado en ordenar: " << diff.count() << " segundos" << endl;
}


/* el metodo de ordenar quicksort es uno notablemente mas rapido que el por
   selección, pero tiene una complejidad mas alta */
void Sorting::Quicksort(int tam, int *list, string show) {
  int tope, ini, fin, pos;
  int pilamenor[100];
  int pilamayor[100];
  int izq, der, aux, band;
  int list2[tam];

  cout << "Iniciando ..." << endl;
  /* se pasa el listado a otra lista, así se puede modificar y el originial se
     mantiene intacto, por si se quiere probar el otro tipo de ordenar */
  for(int x = 0; x < tam; x++){
    list2[x] = list[x];
  }

  cout << "Ordenando ..." << endl;
  /* Tiempo inicial de ordenar, de acá inicia el ordenar los numeros */
  chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();

  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = tam-1;

  while (tope >= 0) {
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    tope = tope - 1;

    izq = ini;
    der = fin;
    pos = ini;
    band = true;

    while (band == true) {
      while ((list2[pos] <= list2[der]) && (pos != der)) {
        der = der - 1;
      }
      if (pos == der) {
        band = false;
      }
      else{
        aux = list2[pos];
        list2[pos] = list2[der];
        list2[der] = aux;
        pos = der;

        while ((list2[pos] >= list2[izq]) && (pos != izq)) {
          izq = izq + 1;
        }
        if (pos == izq) {
          band = false;
        }
        else{
          aux = list2[pos];
          list2[pos] = list2[izq];
          list2[izq] = aux;
          pos = izq;
        }
      }
    }
    if (ini < (pos - 1)) {
      tope = tope + 1;
      pilamenor[tope] = ini;
      pilamayor[tope] = pos - 1;
    }
    if (fin > (pos + 1)) {
      tope = tope + 1;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }
  /* fin de ordenar, se toma el tiempo de nuevo */
  chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
  /* Se resta el tiempo final con el inicial, obteniendo el tiempo que se demoró */
  chrono::duration<double> diff = end-start;
  /* en el caso que el usuario le guste ver la lista ordenada, se le da la
     opción en el inicio, si el usuario dice Y o y, se mostrará toda la lista,
     pero mostrar la lista no afectará el tiempo en que se demoró en ordnelarla */
  if (show == "Y" || show == "y") {
    cout << "Ordenado por Quicksort: " << endl;
    printlist(tam, list2);
  }
  cout << "Tiempo demorado en ordenar: " << diff.count() << " segundos" << endl;
}
