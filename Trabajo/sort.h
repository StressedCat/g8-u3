#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <cctype>
using namespace std;

#ifndef SORTING_H
#define SORTING_H

class Sorting{
  private:

  public:
  Sorting();
  void printlist(int tam, int *list);
  void Selection(int tam, int *list, string show);
  void Quicksort(int tam, int *list, string show);
};
#endif
