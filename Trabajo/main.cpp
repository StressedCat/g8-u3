#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <cctype>
using namespace std;

#include "sort.h"


int main() {
  bool flick = false;
  int opt;
  int tam;
  int num;
  bool show;
  string showlist;
  Sorting sort;
  bool rango = false;
  /* el usuario decidirá cual será el tamaño de su listado, los numeros serán
     generados al azar dependiendo del tamaño */
  cout << "Cual será el tamaño maximo de el listado (1 hasta 1.000.000)" << endl;
  while (rango == false) {
    cin >> tam;
    if (tam > 0 && tam <= 1000000) {
      rango = true;
    }
    else{
      cout << "El numero se encuentra fuera del rango" << endl;
    }
  }
  int list[tam];

  /* se llena la lista con numeros al azar */
  srand(time(NULL));
  for (int x = 0; x < tam; x++) {
    num = (rand() % 4999 + 1);
    list[x] = num;
  }

  /* Menu donde será infinito hasta que el usuario ingrese 0 */
  while(flick != true){
      cout << "=============================================" << endl;
      cout << "[1] Selection" << endl;
      cout << "[2] Quicksort" << endl;
      cout << "[3] Ver vector" << endl;
      cout << "[0] Para terminar el programa" << endl;
      cout << "=============================================" << endl;
      cout << "Ingrese su opción: " << endl;
      cin >> opt;

      /* opt ordenar por selección, en donde el usuario podra decidir si quiere
         ver el listado ordenado */
      if (opt == 1) {
        cout << "¿Desea ver los numeros ordenados?" << endl;
        cout << "Y = si" << endl << "N = no" << endl;
        show = false;
        while (show != true) {
          cin >> showlist;
          if (showlist == "Y" || showlist == "y") {
            show = true;
          }
          else if (showlist == "N" || showlist == "n"){
            show = true;
          }
          else{
            cout << "Ingreso incorrecto" << endl;
          }
        }
        sort.Selection(tam, list, showlist);
      }

      /* opt ordenar por quicksort, en donde el usuario podra decidir si quiere
         ver el listado ordenado */
      else if (opt == 2) {
        cout << "¿Desea ver los numeros ordenados?" << endl;
        cout << "Y = si" << endl << "N = no" << endl;
        show = false;
        while (show != true) {
          cin >> showlist;
          if (showlist == "Y" || showlist == "y") {
            show = true;
          }
          else if (showlist == "N" || showlist == "n"){
            show = true;
          }
          else{
            cout << "Ingreso incorrecto" << endl;
          }
        }
        sort.Quicksort(tam, list, showlist);
      }

      /* opt printlist, se puede ver cuales son los numeros obtenidos en el
         orden que se obtuvieron, en otras palabras, no ordenados */
      else if (opt == 3) {
        sort.printlist(tam, list);
      }

      /* opt terminar el programa */
      else if (opt == 0){
        flick = true;
      }
      /* Ingreso de valor inexistente */
      else{
        cout << "Su ingreso es incorrecto, intente de nuevo" << endl;
      }
    }

  return 0;
}
