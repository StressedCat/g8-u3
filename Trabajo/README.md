# G8-U3

Requisitos:
  C++

Para iniciar el programa:
  1.- Ingrese en el CMD el comando make para instalar el programa
  -> .../g8-u3/Trabajo$ make  
  2.- Se creará un programa con nombre de Cargo, para ingresar a este
  haga lo siguiente:
  -> .../g8-u3/Trabajo$ ./Sorting

En el programa:
  El programa consiste en ordenar la lista por medio de dos metodos, quicksort
  y selection, la función de hacerlo con ambos consiste en ver cuanto tiempo
  demora cada uno de ambos dejando que el usuario decida cuantos numeros desea
  entre 1 hasta 1.000.000 numeros ingresados al azar.

Para borrar el programa:
  Asegurese que esté en la misma carpeta donde corrio el programa
  -> .../g8-u3/Trabajo
  Ingrese el comando make clean para borrar el programa que uso
  -> .../g8-u3/Trabajo$ make clean
